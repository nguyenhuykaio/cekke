
import LogoWeb from '~/assets/images/header/logo.png'

export const EXP_PRIMARY_TAB = [
  {
    title: "Experience",
    value: 2,
    href: "",
    img: LogoWeb,
    imgActive: LogoWeb,
    isMobile: false,
  },
  {
    title: "NFT Wallet",
    value: 4,
    href: "",
    img: LogoWeb,
    imgActive: LogoWeb,
  },
  {
    title: "Mint NFT",
    value: 3,
    href: "",
    img: LogoWeb,
    imgActive: LogoWeb,
  },
]

