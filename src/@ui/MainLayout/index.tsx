import React, { useEffect, useMemo, useState } from 'react'
import { Box, Flex, useColorMode, Image, Text, HStack, VStack, Tooltip, Stack, } from '@chakra-ui/react'
import {
  ProSidebar,
  SidebarHeader,
  SidebarContent,
} from 'react-pro-sidebar'
import useScrollPosition from '@react-hook/window-scroll'

import IconScrollTop from '~/assets/images/go_top.png'
import { codeHelpers } from '~/@helpers/code.helpers'
import { useLocation, useNavigate } from 'react-router-dom'
import { ReactComponent as CollapsedIcon } from '~/assets/svgs/collapse-menu-icon.svg'
import LogoWeb from '~/assets/images/header/logo.png'
import IconLogout from '~/assets/images/logout.png'

import { useWindowSize } from '~/hooks/@global'
import { EXP_PRIMARY_TAB } from '~/@constants/PrimaryTab';
import { FixedHeader } from '../../components/Header/FixedHeader'


const MainLayout = ({ children }) => {

  const { colorMode, } = useColorMode();
  const { width } = useWindowSize();
  const history = useNavigate();
  const scrollY = useScrollPosition(60 /*fps*/)
  const { pathname } = useLocation();
  const [collapsed, setCollapsed] = useState(false);

  useEffect(() => {
    if (width <= 1100) {
      setCollapsed(true);
    }
    codeHelpers.scrollToTop();
  }, [width])

  const renderProSidebar = useMemo(() => {
    if (width <= 992) {
      return null;
    }
    return (
      <>
        <Box
          style={{
            transition: 'left 0.3s',
          }}
          position="fixed"
          top="100px"
          left={collapsed ? '68px' : '260px'}
          zIndex="1010"
          display={width <= 768 ? 'none' : 'block'}
          cursor={"pointer"}
          onClick={() => {
            setCollapsed((currentState) => !currentState)
          }}
        >
          <CollapsedIcon bg="#EDECF0" />
        </Box>

        <Box
          h="100vh"
          position={{ base: 'absolute', md: 'relative' }}
          zIndex={20}
          background={`${colorMode}.bgProSider`}
          backdropFilter="saturate(180%) blur(5px)"
          boxShadow={"0 1px 2px 0 rgba(0, 0, 0, 0.05)"}
          borderStyle={"solid"}
          borderRightWidth={"1px"}
          borderRightColor={`1px solid ${colorMode}.borderColor`}
        >
          <ProSidebar
            collapsed={collapsed}
            collapsedWidth={0}
            style={{
              background: `inherit`,
              backdropFilter: "inherit"
            }}
          >

            <Box h="80px" />

            {/* <SidebarHeader>
              <Box display="flex" justifyContent="center" alignItems="center" height={"80px"} cursor={"pointer"}>
                <Image
                  src={LogoWeb}
                  h="40px"
                  objectFit={"contain"}
                  onClick={() => {
                    history('/')
                  }}
                />
              </Box>
            </SidebarHeader > */}

            <SidebarContent>
              <VStack
                w="full"
                padding={{ lg: collapsed ? "20px 0px" : "34px 20px" }}
              >

                {EXP_PRIMARY_TAB.map((item, idx) => {
                  const isActive = codeHelpers.checkIsFocusUrl(pathname, item.value === 3 ? 'mint-nft' : item.href);
                  return (
                    <Tooltip
                      key={idx}
                      label={item.title}
                      hasArrow
                      placement='right'
                    >
                      <VStack
                        w={collapsed ? "auto" : "full"}
                        alignItems={collapsed ? "center" : "start"}
                        onClick={() => {
                          history(`/${item.href}`)
                        }}
                      >

                        <HStack
                          w="full"
                          spacing={2}
                          cursor="pointer"
                          // padding={"8px"}
                          borderRadius="24px"
                          color={`${colorMode}.textPrimary`}
                          background={"transparent"}
                          _hover={{
                            opacity: "0.8",
                          }}
                        >
                          <Image
                            src={isActive ? item.imgActive : item.img}
                            width={"40px"}
                            height={"40px"}
                            objectFit={"cover"}
                            p="10px"
                          />

                          {!collapsed &&
                            <Text
                              fontSize={{ base: "16px" }}
                              lineHeight={{ base: "26px" }}
                              textTransform={"capitalize"}
                              color={`${colorMode}.textPrimary`}
                            >
                              {item.title}
                            </Text>
                          }

                        </HStack>
                      </VStack>
                    </Tooltip>
                  )
                })}

              </VStack>

            </SidebarContent>
          </ProSidebar>

          <Box
            style={{
              opacity: collapsed ? 0 : 1,
              display: collapsed ? 'none' : 'block',
              transition: 'opacity 0.2s',
            }}
            p="16px"
            position="absolute"
            bottom="0px"
            left="0px"
            zIndex={1010}
            cursor={"pointer"}
            onClick={() => {
            }}
          >
            <HStack
              spacing="13px"
              padding="6px 8px"
              borderRadius="60px"
              w="185px"
            >
              <Image src={IconLogout} w="20px" />
              <Text
                color={`${colorMode}.textPrimary`}
                fontSize={{ base: "20px" }}
                lineHeight={{ base: "29px" }}
                fontFamily={"Encode-Medium"}
              >
                Log out
              </Text>
            </HStack>

          </Box>

        </Box>
      </>
    )
  }, [collapsed, colorMode, history, pathname, width])

  return (
    <Box
      background={`${colorMode}.bgPrimary`}
      position="relative"
      overflow="hidden"
      h="100vh"
      w="100%"
    >

      <Box id="headerScroll">
        <FixedHeader
          collapsed={collapsed}
          setCollapsed={(val: boolean) => setCollapsed(val)}
        />
      </Box>

      <Stack
        direction={{
          base: 'column',
          lg: 'row',
        }}
        h="100vh"
        overflow="hidden"
      >

        {renderProSidebar}

        <Box
          pt="100px"
          height="100%"
          overflowY="auto"
          overflowX="hidden"
          width={"100%"}
        >
          {children}
        </Box>

      </Stack>


      {scrollY > 1300 && width >= 768 &&
        <Box
          zIndex={99}
          cursor={"pointer"}
          position={"fixed"}
          bottom="3%"
          right={{ base: "1%", }}
          background={`${colorMode}.bgPrimary`}
          borderRadius="100px"
          onClick={() => {
            codeHelpers.scrollToTop();
          }}
        >
          <Image
            src={IconScrollTop}
            height="50px"
          />
        </Box>
      }

    </Box>
  )
}

export default MainLayout
