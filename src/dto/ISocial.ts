
export interface ISocial {
    id: string
    title: string;
    name: string;
    icon: string;
    href: string;
}