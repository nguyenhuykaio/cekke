
const stringDateToUTCDate = (dateStr: string) => {
    const inputDate = new Date(dateStr);
    return new Date(inputDate.getTime() - inputDate.getTimezoneOffset() * 60000);
}

const scrollToTop = () => {
    const element = document.getElementById("headerScroll");
    element.scrollIntoView({ behavior: "smooth", block: 'center' })
}

const checkIsFocusUrl = (pathname: string, key: string) => {
    const url = pathname.split("/");

    if (pathname === "/") {
        return true;
    }
    if (url.includes(key)) {
        return true;
    }
    return false;
}


const pipeLongTextUi = (value: string = "", leftCharAmount = 4, rightCharAmount = 4) => {
    if (value.length <= leftCharAmount + rightCharAmount + 3) {
        return value;
    }
    return `${value?.substring(0, leftCharAmount) ?? ''}...
    ${value?.substring(value.length - rightCharAmount) ?? ''}`
}


export const codeHelpers = {
    stringDateToUTCDate,
    scrollToTop,
    checkIsFocusUrl,
    pipeLongTextUi,
}