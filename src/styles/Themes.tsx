import { extendTheme, theme } from '@chakra-ui/react'

const breakpoints = {
  base: 0,
  '320': '20em',
  '375': '23.4375em',
  sm: '30em',
  md: '48em',
  lg: '62em',
  '2lg': '64em',
  '3lg': '68.75em',
  xl: '80em',
  xxl: '89em',
  '2xl': '96em',
  '3xl': '120em',
}

const customTheme = extendTheme({
  fonts: {
    body: `'Encode', sans-serif`,
  },
  config: {
    initialColorMode: 'light',
    useSystemColorMode: false,
  },
  colors: {
    light: {
      textPrimary: "#070814",
      subText: "#737373",

      primary: '#4194FF',

      logout: '#FF3D3D',
      footer: '#A5A5A5',

      bgPrimary: '#fff',
      bgSecondary: '#fff',
      bgProSider: 'rgba(255, 255, 255, 0.8)', // 1a202c

      borderColor: "#171923",
      boxShadowPrimary: '0px 0px 10px 0px #4194FF',

    },
    dark: {

      textPrimary: "#fff",
      subText: "#737373",

      primary: '#4194FF',
      logout: '#FF3D3D',
      footer: '#A5A5A5',

      bgPrimary: '#1a202c',
      bgSecondary: '#1a202c',
      bgProSider: 'rgba(26, 32, 44, 0.8)', // 1a202c
      borderColor: '#E2E8F0',
      boxShadowPrimary: '0px 0px 10px 0px #4194FF',
    },
  },
  shadows: {
    ...theme.shadows,
    outline: 'none',
  },
  breakpoints,
})

export default customTheme
