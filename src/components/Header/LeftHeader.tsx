import { useColorMode, Text, VStack, HStack } from '@chakra-ui/react'
import { Fragment, memo } from 'react'
import { useWindowSize } from '~/hooks/@global'
import TeleIcon from '~/assets/footer/icon_white_telegram.svg'
import TwitterIcon from '~/assets/footer/icon_white_twitter.svg'
import YbIcon from '~/assets/footer/icon_white_youtube.svg'
import { SocialsLayout } from '../SocialsLayout'
import { LinkTextRedirect } from './LinkTextRedirect'
import { ChangingColorMode } from './ChangingColorMode'

export const LeftHeader = memo(() => {
  const { width } = useWindowSize()
  const { colorMode, } = useColorMode();


  return (
    <Fragment>

      <LinkTextRedirect />

      <HStack
        px="16px"
        pt="10px"
        spacing={4}
      >

        <Text
          fontSize="16px"
          color={`${colorMode}.textPrimary`}
        >
          Color Mode
        </Text>

        <ChangingColorMode />
      </HStack>

      <VStack
        px="16px"
        pt="30px"
        alignItems={{ base: "center", }}
      >
        <SocialsLayout
          socialLinks={[
            {
              id: "1",
              title: "Twitter",
              name: "Twitter",
              icon: TwitterIcon,
              href: "",
            },
            {
              id: "2",
              title: "Telegram Group",
              name: "Telegram Group",
              icon: TeleIcon,
              href: "",
            },
            {
              id: "3",
              title: "Telegram Channel",
              name: "Telegram Channel",
              icon: TeleIcon,
              href: "",
            },
            {
              id: "4",
              title: "Youtube",
              name: "Youtube",
              icon: YbIcon,
              href: "",
            },
          ]}
        />
      </VStack>

    </Fragment>
  )
})
