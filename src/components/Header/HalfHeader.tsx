import { Fragment, useCallback, useMemo, } from 'react'
import {
  Box,
  HStack,
  useDisclosure,
  Drawer,
  DrawerContent,
  DrawerBody,
  Image,
  SimpleGrid,
  Flex,
  useColorMode,
  VStack,
} from '@chakra-ui/react'

import LogoWeb from '~/assets/images/header/logo.png'
import HamburgerIcon from '~/assets/svgs/hamburger-menu.svg'
import CloseIcon from '~/assets/svgs/close-menu.svg'
import { useLocation, useNavigate } from 'react-router-dom'
import { useWindowSize } from '~/hooks/@global'
import { LinkTextRedirect } from './LinkTextRedirect'
import { LeftHeader } from './LeftHeader'
import { PopoverWeb3 } from './PopoverWeb3'
import { ChangingColorMode } from './ChangingColorMode'

const MobileDrawer = ({ onClose, isOpen, }) => {
  const { pathname } = useLocation();
  const history = useNavigate();
  const { colorMode, } = useColorMode();

  return (
    <Drawer placement="left" onClose={onClose} isOpen={isOpen}>
      <DrawerContent mt="65px" w={"190px !important"} background={`${colorMode}.bgPrimary`}>
        <DrawerBody p="0px">

          <LeftHeader />

        </DrawerBody>
      </DrawerContent>
    </Drawer >
  )
}

export const HalfHeader = (props: {
  setCollapsed: (val: boolean) => void
  collapsed: boolean,
}) => {
  const history = useNavigate();
  const { width } = useWindowSize()
  const { isOpen, onOpen, onClose, } = useDisclosure()
  const { colorMode } = useColorMode();
  const { setCollapsed, collapsed } = props;

  const renderIcon = useCallback(() => {

    const onClickIcon = () => {
      setCollapsed(!collapsed);
      if (isOpen) {
        onClose();
      } else {
        onOpen();
      }
    }

    return (
      <HStack>
        <Image
          src={
            isOpen ? HamburgerIcon : CloseIcon
          }
          fontSize="22px"
          onClick={onClickIcon}
        />
        {/* <PopoverChains /> */}
      </HStack>
    )
  }, [isOpen, onClose, onOpen])

  const renderHeader = useMemo(() => {

    if (width <= 992) {
      return (
        <Box
          py="15px"
          px={{
            base: '5px',
          }}
          h={{
            base: '70px',
          }}
          background={`${colorMode}.bgPrimary`}
          boxShadow={"0px 4px 26px 0px #4194FF3D"}
          zIndex={99}
          w="100%"

          position="fixed"
        >
          <SimpleGrid
            spacing={0}
            columns={3}
            maxW={{ base: "100%", }}
          >

            {renderIcon()}

            <VStack>
              <Image
                src={LogoWeb}
                h="40px"
                objectFit={"contain"}
                onClick={() => {
                  history('/')
                }}
              />
            </VStack>

            <VStack alignItems={"end"}>
              <PopoverWeb3 />
            </VStack>

          </SimpleGrid>
        </Box>
      )
    }
    return (
      <Box
        w="full"
      >
        <Flex
          h="100%"
          alignItems={'center'}
          justifyContent={"space-between"}
          maxH="80px"
          w="full"
          padding={{ base: "15px", xl: "24px 80px" }}
          borderBottomWidth={"1px"}
          borderBottomColor={`1px solid ${colorMode}.borderColor`}
          background={`${colorMode}.bgPrimary`}
        >

          <Box w={{ base: "0px", lg: "100px", '3xl': "200px" }} />

          <HStack>

            <LinkTextRedirect />
            {/* <PopoverChains /> */}
            <ChangingColorMode />
            <PopoverWeb3 />
          </HStack>

        </Flex>
      </Box>
    )
  }, [colorMode, history, renderIcon, width])

  return (
    <Fragment>

      {renderHeader}

      <MobileDrawer
        onClose={onClose}
        isOpen={isOpen}
      />
    </Fragment>
  )
}

