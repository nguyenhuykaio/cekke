import { Fragment, useCallback, useMemo, } from 'react'
import {
  Box,
  HStack,
  useDisclosure,
  Drawer,
  DrawerContent,
  DrawerBody,
  Image,
  SimpleGrid,
  VStack,
  Flex,
  useColorMode,
  Text,
  Icon
} from '@chakra-ui/react'

import LogoWeb from '~/assets/images/header/full_logo.png'

import { ReactComponent as HamburgerIcon } from '~/assets/svgs/hamburger-menu.svg'
import { ReactComponent as CloseIcon } from '~/assets/svgs/close-menu.svg'


import { useLocation, useNavigate } from 'react-router-dom'
import { useWindowSize } from '~/hooks/@global'
import useScrollPosition from '@react-hook/window-scroll'
import { PopoverWeb3 } from './PopoverWeb3'
import { LeftHeader } from './LeftHeader'
import { LinkTextRedirect } from './LinkTextRedirect'
import { ChangingColorMode } from './ChangingColorMode'

const MobileDrawer = ({ onClose, isOpen, }) => {
  const { colorMode, } = useColorMode();

  return (
    <Drawer placement="left" onClose={onClose} isOpen={isOpen}>
      <DrawerContent mt="55px" w={"190px !important"} background={`${colorMode}.bgPrimary`}>
        <DrawerBody p="20px 0px 0px">

          <LeftHeader />

        </DrawerBody>
      </DrawerContent>
    </Drawer >
  )
}

export const Header = (props: {}) => {
  const history = useNavigate();
  const { colorMode, toggleColorMode } = useColorMode();
  const { width } = useWindowSize()
  const { isOpen, onOpen, onClose, } = useDisclosure()
  const { pathname } = useLocation();
  const scrollY = useScrollPosition(60 /*fps*/);

  const checkScrollAtHome = useMemo(() => {
    return scrollY > 176;
    // if (pathname !== "/") {
    //   return true;
    // }
    // return pathname === "/" && scrollY > 176;
  }, [scrollY])

  const renderIcon = useCallback(() => {
    return (
      <HStack>
        <Icon
          as={
            isOpen ? HamburgerIcon : CloseIcon
          }
          fontSize="22px"
          onClick={isOpen ? onClose : onOpen}
          color={`${colorMode}.textPrimary`}
        />
        {/* <PopoverChains /> */}
      </HStack>
    )
  }, [colorMode, isOpen, onClose, onOpen])

  const renderHeader = useMemo(() => {

    if (width <= 992) {
      return (
        <Box
          py="15px"
          px={{
            base: '5px',
          }}
          h={{
            base: '58px',
          }}
          background={checkScrollAtHome ? `${colorMode}.bgPrimary` : "transparent"}
          boxShadow={checkScrollAtHome ? "0px 4px 26px 0px #4194FF3D" : "none"}
          zIndex={99}
          w="100%"

          position="fixed"
        >
          <SimpleGrid
            spacing={0}
            columns={3}
            maxW={{ base: "100%", }}
          >

            {renderIcon()}

            <VStack>
              <Image
                src={LogoWeb}
                h="40px"
                objectFit={"contain"}
                onClick={() => {
                  history('/')
                }}
              />
              {/* <Box
                maxHeight={"0px"}
                onClick={() => {
                  history("/")
                }}
              >
                <svg>
                  <text className="dashed" x="30px" y="0" dominant-baseline="hanging" text-anchor="middle" style={{ fontSize: "24px" }}>
                    <tspan x="110px" dy="0">MinT-Rex</tspan>
                  </text>
                </svg>
              </Box> */}
            </VStack>

            <VStack alignItems={"end"}>
              <PopoverWeb3 />
            </VStack>

          </SimpleGrid>
        </Box>
      )
    }
    return (
      <VStack
        position="fixed"
        zIndex={99}
        w="100%"
        background={checkScrollAtHome ? `${colorMode}.bgPrimary` : "transparent"}
        boxShadow={checkScrollAtHome ? "0px 4px 26px 0px #4194FF3D" : "none"}
      >

        <Flex
          h="100%"
          alignItems={'center'}
          justifyContent={'space-between'}
          maxH="800px"
          w="full"
          maxW={{ base: "100%", lg: "1100px", xl: "1200px" }}
          padding={{ base: "15px 12px", lg: "12px 32px" }}
        >

          <Image
            src={LogoWeb}
            cursor="pointer"
            onClick={() => {
              history("/")
            }}
            h="50px"
            objectFit={"contain"}
          />

          {/* <Box
            cursor="pointer"
            onClick={() => {
              history("/")
            }}
            height={"30px"}
          >
            <svg>
              <text className="dashed" x="0" y="0" dominant-baseline="hanging" text-anchor="middle" style={{ fontSize: "30px" }}>
                <tspan x="120px" dy="0">MinT-Rex</tspan>
              </text>
            </svg>
          </Box> */}

          <HStack spacing={6}>
            <LinkTextRedirect />
            <ChangingColorMode />
            <PopoverWeb3 />
          </HStack>

        </Flex>

      </VStack>
    )
  }, [checkScrollAtHome, colorMode, history, renderIcon, width])

  return (
    <Fragment>

      {renderHeader}

      <MobileDrawer
        onClose={onClose}
        isOpen={isOpen}
      />
    </Fragment>
  )
}
