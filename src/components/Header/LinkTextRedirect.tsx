import {
  Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Box, HStack, Popover,
  PopoverArrow, PopoverBody, PopoverContent, PopoverTrigger, Text, useColorMode, VStack
} from '@chakra-ui/react'
import { Fragment, memo, useCallback, useMemo, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import { useWindowSize } from '~/hooks/@global';
import useScrollPosition from '@react-hook/window-scroll'
import { RoutersPath } from '~/@constants/RoutersPath';

interface IPropsChild {
  key: string,
  title: string,
  link: string,
}
interface IProps {
  key: string,
  title: string,
  link: string,
  activeKeys: string[],
  childs: IPropsChild[],
}

const DATA_LINK: IProps[] = [
  {
    key: RoutersPath.REX_POINT,
    title: "Rex Point",
    link: RoutersPath.REX_POINT,
    activeKeys: [],
    childs: [
    ],
  },
  {
    key: RoutersPath.SOCIALS_TASK,
    title: "Social Task",
    link: RoutersPath.SOCIALS_TASK,
    activeKeys: [],
    childs: [
    ],
  },
]

export const LinkTextRedirect = memo(() => {
  const { colorMode, } = useColorMode();
  const { width } = useWindowSize()
  const history = useNavigate();
  const { pathname } = useLocation();
  const scrollY = useScrollPosition(60 /*fps*/);
  const [isOpen, setIsOpen] = useState(false);
  const [selectMenu, setSelectMenu] = useState(null);

  const checkScrollAtHome = useMemo(() => {
    if (pathname !== "/") {
      return true;
    }
    return pathname === "/" && scrollY > 176;
  }, [pathname, scrollY])

  const menuSelect = useMemo(() => {
    if (selectMenu) {
      return DATA_LINK.find((i, idx) => i.key === selectMenu)
    }
    return null;
  }, [selectMenu])

  const onClickHandle = (item: IPropsChild) => {
    if (item.link) {
      if (item.link.includes("http")) {
        window.open(item.link);
      } else {
        history("/" + item.link)
      }
    }
  }

  const renderPopover = useCallback((item: IProps) => {
    if (!item.childs.length) {
      return (
        <Text
          cursor={"pointer"}
          color={{
            base: item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.primary` : `${colorMode}.textPrimary`,
            lg: checkScrollAtHome ? (item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.primary` : `${colorMode}.textPrimary`) : `${colorMode}.textPrimary`
          }}
          _hover={{
            color: `${colorMode}.primary`
          }}
          fontSize={{ base: "16px", }}
          lineHeight={{ base: "21px" }}
          fontFamily={"Encode-Medium"}
          onClick={() => onClickHandle(item)}
        >
          {item.title}
        </Text>
      )
    }
    return (
      <Popover
        placement="bottom"
        isOpen={isOpen}
        onClose={() => {
          setIsOpen(false);
          setSelectMenu(null);
        }}
      >
        <PopoverTrigger>
          <HStack
            spacing={4}
            cursor={"pointer"}
            w="fit-content"
          >
            <Text
              cursor={"pointer"}
              color={{
                base: item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.primary` : `${colorMode}.textPrimary`,
                lg: checkScrollAtHome ? (item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.primary` : `${colorMode}.textPrimary`) : `${colorMode}.textPrimary`
              }}
              _hover={{
                color: `${colorMode}.primary`
              }}
              fontSize={{ base: "16px", lg: "16px" }}
              lineHeight={{ base: "21px" }}
              fontFamily={"Encode-Medium"}
            >
              {item.title}
            </Text>
          </HStack>
        </PopoverTrigger>

        {menuSelect && item.key === selectMenu &&

          <PopoverContent
            w={{ base: "200px", }}
            background={`${colorMode}.bgSecondary`}
            borderColor={"#EAEAEA !important"}
            borderRadius="8px"
            p="10px 5px"
            onMouseLeave={() => {
              setIsOpen(false);
              setSelectMenu(null);
            }}
          >

            <PopoverBody>

              <PopoverArrow bg='#fff' />

              {menuSelect.childs.map((item, idx) => {
                return (
                  <HStack
                    key={idx}
                    alignItems={"center"}
                    cursor={"pointer"}
                    p="14px 5px"
                    borderRadius={"5px"}
                    color={{
                      base: pathname.split("/")[1] === item.key ? `${colorMode}.primary` : `${colorMode}.textPrimary`,
                      lg: checkScrollAtHome ? (pathname.split("/")[1] === item.key ? `${colorMode}.primary` : `${colorMode}.textPrimary`) : `${colorMode}.textPrimary`
                    }}
                    fontFamily={"Encode-Medium"}
                    _hover={{
                      opacity: ".8",
                      color: `${colorMode}.primary`,
                    }}
                    onClick={() => onClickHandle(item)}
                  >
                    <Text
                      fontSize="13px"
                      lineHeight="16px"
                      textTransform={"capitalize"}
                    >
                      {item.title}
                    </Text>
                  </HStack>
                )
              })}

            </PopoverBody>
          </PopoverContent>
        }

      </Popover>
    )
  }, [checkScrollAtHome, colorMode, isOpen, menuSelect, onClickHandle, pathname, selectMenu])

  const renderMobile = () => {
    return (
      <Accordion
        w="full"
      >
        {DATA_LINK.map((item, idx) => {
          if (!item.childs.length) {
            return (
              <Text
                p="8px 16px"
                textAlign={"left"}
                fontSize={{ base: "16px", }}
                fontFamily={"Encode-Medium"}
                color={{
                  base: item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.secondary` : `${colorMode}.textPrimary`,
                  lg: checkScrollAtHome ? (item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.secondary` : `${colorMode}.textPrimary`) : `${colorMode}.textPrimary`
                }}
                letterSpacing={"-0.05em"}
                onClick={() => onClickHandle(item)}
              >
                {item.title}
              </Text>
            )
          }
          return (
            <AccordionItem
              key={idx}
              borderTop={"none"}
              border="none"
              borderBottomWidth={"none"}
              style={{
                borderBottomWidth: "0px !important"
              }}
            >
              <AccordionButton
                alignItems="start"
                borderBottomWidth={"none"}
                borderRadius="5px"
                backgroundColor="transparent"
                background="transparent"
                border="1px solid transparent"
                transition="0.4s"
                boxShadow="0px 0px 0px 0px rgba(0, 0, 0, 0)"
                style={{
                  borderBottomWidth: "0px !important"
                }}
              >
                <HStack w="full" justifyContent={"space-between"} alignItems="start">
                  <Text
                    textAlign={"left"}
                    fontSize={{ base: "16px", }}
                    fontFamily={"Encode-Bold"}
                    color={{
                      base: item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.secondary` : `${colorMode}.textPrimary`,
                      lg: checkScrollAtHome ? (item.activeKeys.includes(pathname.split("/")[1]) ? `${colorMode}.secondary` : `${colorMode}.textPrimary`) : `${colorMode}.textPrimary`
                    }}
                    letterSpacing={"-0.05em"}
                  >
                    {item.title}
                  </Text>
                  <AccordionIcon color={`#fff`} />
                </HStack>

              </AccordionButton>

              <AccordionPanel
                style={{
                  borderBottomWidth: "0px !important"
                }}
              >
                <VStack
                  spacing={4}
                  alignItems={"start"}
                  paddingLeft={"10px"}
                >
                  {item.childs.map((item, idx) => {
                    return (
                      <Text
                        key={idx}
                        fontSize={{ base: "16px", }}
                        color={{
                          base: pathname.split("/")[1] === item.key ? `${colorMode}.secondary` : `${colorMode}.textPrimary`,
                          lg: checkScrollAtHome ? (pathname.split("/")[1] === item.key ? `${colorMode}.secondary` : `${colorMode}.textPrimary`) : `${colorMode}.textPrimary`
                        }}
                        fontFamily={"Encode-Medium"}
                        onClick={() => onClickHandle(item)}
                      >
                        {item.title}
                      </Text>
                    )
                  })}
                </VStack>
              </AccordionPanel>
            </AccordionItem>
          )
        })}

      </Accordion>
    )
  }

  if (width <= 992) {
    return <VStack
      w="full"
      spacing={7}
      alignItems={"start"}
    >
      {renderMobile()}
    </VStack>
  }

  return <HStack spacing={6}>
    {DATA_LINK.map((item, idx) => {
      return (
        <Box
          key={idx}
          onMouseOver={() => {
            setIsOpen(true);
            setSelectMenu(item.key);
          }}
          onClick={() => {
            setIsOpen(true);
            setSelectMenu(item.key);
          }}
        >
          {renderPopover(item)}
        </Box>
      )
    })}
  </HStack>
})
