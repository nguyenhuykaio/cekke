import {
  Popover, PopoverTrigger,
  Box, HStack, PopoverContent,
  PopoverBody, Text,
  useColorMode, Image, Icon, VStack
} from "@chakra-ui/react";
import IconLogOut from '~/assets/images/logout.png'
import IconLogo from '~/assets/images/header/logo.png'
import { useWindowSize } from "~/hooks/@global";
import { ConnectWalletButton } from "../ConnectWalletButton";
import ModalConnectWallet from "../ModalConnectWallet";
import { useState } from "react";
import { codeHelpers } from "~/@helpers/code.helpers";
import { ReactComponent as IconCopy } from '~/assets/svgs/icon_copy.svg'

export const PopoverWeb3 = () => {
  const { width } = useWindowSize();
  const { colorMode, } = useColorMode();

  const [isOpen, setIsOpen] = useState(false)

  const renderButtonProfilePopover = () => {
    return (
      <Popover>
        <PopoverTrigger>

          <Box
            borderRadius="100px"
            padding={{ base: "5px", md: "5px 15px" }}
            cursor={"pointer"}
            borderStyle={"dashed"}
            borderWidth={"1px"}
            borderColor={`${colorMode}.borderColor`}
          >
            <HStack maxW={"160px"}>
              <Text
                color={`${colorMode}.textPrimary`}
                alignSelf={"center"}
                fontSize={"13px"}
              >
                {codeHelpers.pipeLongTextUi("0x68a44D9306084DDBc7b53142A218cc9cAb795c63")}
              </Text>
              <Image src={IconLogo} h={{ base: "18px", md: "25px" }} />
            </HStack>
          </Box>

        </PopoverTrigger>

        <PopoverContent
          mr="5px"
          mt="10px"
          w="180px"
          background={`${colorMode}.bgPrimary`}
          borderStyle={"dashed"}
          borderWidth={"1px"}
          borderColor={`${colorMode}.textPrimary`}
          borderRadius="24px"
          padding={"12px 18px"}
          boxShadow="0px 0px 10px 0px #4194FF"
        >
          <PopoverBody>

            <VStack w="full" spacing={4} alignItems={"start"}>

              <HStack
                cursor={"pointer"}
                _hover={{
                  borderRadius: "8px",
                  background: `${colorMode}.bgPopupSetting`
                }}
                onClick={() => {
                }}
              >
                <Icon as={IconCopy} height="16px" width={"16px"} color={`${colorMode}.textPrimary`} />
                <Text
                  fontSize="14px"
                  textTransform={"capitalize"}
                  color={`${colorMode}.textPrimary`}
                >
                  Copy
                </Text>
              </HStack>

              <HStack
                cursor={"pointer"}
                _hover={{
                  borderRadius: "8px",
                  background: `${colorMode}.bgPopupSetting`
                }}
                onClick={() => {
                }}
              >
                <Image src={IconLogOut} height="16px" width={"16px"} />
                <Text
                  fontSize="14px"
                  textTransform={"capitalize"}
                  color={`${colorMode}.logout`}
                >
                  Log out
                </Text>
              </HStack>
            </VStack>

          </PopoverBody>
        </PopoverContent>
      </Popover>
    )
  }

  if (false) {
    return (
      <>
        <ConnectWalletButton
          onClick={() => { setIsOpen(true) }}
          p={{ base: "8px", md: "auto" }}
        >
          Connect Wallet
        </ConnectWalletButton>

        <ModalConnectWallet
          isOpen={isOpen}
          onDismiss={() => setIsOpen(false)}
        />

      </>

    )
  }

  return (

    <Box>
      {renderButtonProfilePopover()}
    </Box>

  );
}