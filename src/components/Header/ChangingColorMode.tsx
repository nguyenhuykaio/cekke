import { HStack, useColorMode, Image } from "@chakra-ui/react"
import DarkModeIcon from '~/assets/svgs/ic_brightness-icon.svg'
import LightModeIcon from '~/assets/svgs/ic_wb_sunny.svg'

export const ChangingColorMode = () => {
    const { colorMode, toggleColorMode } = useColorMode();

    return (
        <HStack onClick={toggleColorMode}>
            {colorMode === "dark" ?
                <Image
                    src={LightModeIcon}
                    w="16px"
                    h="16px"
                    objectFit={"contain"}
                />
                :
                <Image
                    src={DarkModeIcon}
                    w="16px"
                    h="16px"
                    objectFit={"contain"}
                />
            }
        </HStack>
    )
}