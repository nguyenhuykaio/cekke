import React from 'react'
import {
  ModalOverlay,
  Modal,
  ModalContent,
  ModalCloseButton,
  HStack,
  Text,
  ModalHeader,
  Box,
  ModalBody,
  SimpleGrid,
  Divider,
  useColorMode,
  Image,
} from '@chakra-ui/react'

import Metamask from '~/assets/images/wallet/metamask.png'
import WalletConnect from '~/assets/images/wallet/wallet_connect.png'
import RiceConnect from '~/assets/images/wallet/rice.png'
import BitKeep from '~/assets/images/wallet/bitkeep.png'

const listWallet = [
  {
    title: 'Metamask',
    icon: Metamask,
  },
  {
    title: 'Rice Wallet',
    icon: RiceConnect,
  },
  {
    title: 'WalletConnect',
    icon: WalletConnect,
  },
  {
    title: 'BitKeep',
    icon: BitKeep,
  },
]

interface IConnectorWalletModal {
  isOpen?: boolean
  onDismiss?: () => void
  login?: any
}

const ModalConnectWallet: React.FC<IConnectorWalletModal> = ({
  login,
  isOpen,
  onDismiss: onClose = () => {
    return null
  },
  ...rest
}) => {

  const { colorMode, } = useColorMode();

  return (
    <Modal isOpen={isOpen || false} onClose={onClose} isCentered>
      <ModalOverlay
        bg='rgba(0,0,0,0.35)'
        backdropFilter='blur(1px)'
      />
      <ModalContent
        maxWidth="580px"
        w="100%"
        borderRadius="16px"
        mx="15px"
        bg={`${colorMode}.bgPrimary`}
      >
        <ModalHeader bg={`${colorMode}.bgPrimary`} borderRadius="16px" />
        <ModalCloseButton bg={`${colorMode}.bgPrimary`} />

        <ModalBody bg={`${colorMode}.bgPrimary`} borderRadius="16px">
          <Box>
            <Text
              fontSize="22px"
              color={`${colorMode}.textPrimary`}
              textAlign="center"
              textTransform="uppercase"
            >
              Connect to a wallet
            </Text>
          </Box>

          <Divider mt="12px" mb="20px" borderColor={"rgba(83, 86, 251, 0.16)"} />

          <SimpleGrid
            columns={{ base: 1, md: 2 }}
            spacing={2}
          >
            {listWallet.map(({ title, icon, }) => (
              <Box
                cursor="pointer"
                key={title}
                onClick={() => {

                  onClose()
                }}
                bg={`${colorMode}.bgPrimary`}
                role="group"
                borderRadius="16px"
              >
                <HStack
                  borderRadius="16px"
                  boxShadow={"0px 9px 95px rgba(0, 0, 0, 0.05)"}
                  color="#fff"
                  border="1px solid rgba(83, 86, 251, 0.16)"
                  alignItems="center"
                  px={{
                    base: '20px',
                    md: '25px',
                  }}
                  py="12px"
                  spacing={{
                    base: '5px',
                    md: '12px',
                  }}
                >
                  <Image
                    src={icon}
                    fontSize="32px"
                    borderRadius={"100px"}
                  />
                  <Text
                    fontSize={{ base: "16px" }}
                    color={`${colorMode}.textPrimary`}
                    pl="15px"
                  >
                    {title}
                  </Text>
                </HStack>
              </Box>
            ))}
          </SimpleGrid>
        </ModalBody>
      </ModalContent>
    </Modal >
  )
}

export default ModalConnectWallet
