import { Box, ButtonProps, useColorMode } from '@chakra-ui/react';

export const BoxPrimary = ({ children, disabled, ...props }: ButtonProps) => {
  const { colorMode, } = useColorMode();

  return (
    <Box
      alignItems="start"
      borderStyle={"dashed"}
      borderWidth={"1px"}
      borderColor={`${colorMode}.borderColor`}
      borderRadius="8px"
      padding="13px 16px"
      bg="transparent"

      transition="all 200ms ease-in-out"
      transform="translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg)"
      _hover={{
        boxShadow: '0px 0px 10px 0px #4194FF',
        transform: "translate3d(0px, -8px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg)"
      }}
      cursor={"pointer"}
      onClick={() => {

      }}
      {...props}
    >
      {children}
    </Box>
  )
}


