import { Box, HStack, Image, Tooltip, } from '@chakra-ui/react'
import { ISocial } from '~/dto'
import { useWindowSize } from '~/hooks/@global';

export const SocialsLayout = (props: {
  socialLinks: ISocial[]
}) => {
  const { socialLinks } = props;
  const { width } = useWindowSize();
  return (
    <HStack spacing={4}>
      {socialLinks.length > 0 &&
        socialLinks.map((item, idx) => {
          if (width <= 500) {
            return (
              <Box
                key={idx}
                style={{ cursor: 'pointer' }}
                onClick={() => {
                  window.open(item.href, '_blank')
                }}
              >
                <Image
                  src={item.icon}
                  h="28px"
                />
              </Box>
            )
          }
          return (
            <Tooltip
              label={item.name}
              placement='top-start'
              borderRadius="8px"
              key={idx}
              fontFamily={"Encode"}
            >
              <Box
                style={{ cursor: 'pointer' }}
                onClick={() => {
                  window.open(item.href, '_blank')
                }}
              >
                <Image
                  src={item.icon}
                  h="28px"
                />
              </Box>
            </Tooltip>
          )
        })
      }
    </HStack>
  )
}
