import { ButtonProps, useColorMode } from '@chakra-ui/react';
import PrimaryButton from './PrimaryButton';


export const ConnectWalletButton = ({ children, disabled, ...props }: ButtonProps) => {
  const { colorMode, } = useColorMode();

  return (
    <PrimaryButton
      color={`${colorMode}.textPrimary`}
      background={`${colorMode}.bgPrimary`}
      fontWeight={700}
      fontSize={{
        base: "12px",
      }}
      borderStyle={"solid"}
      borderWidth={"1px"}
      borderColor={`${colorMode}.borderColor`}
      borderRadius="8px"
      disabled={disabled}
      _hover={{
        boxShadow: '0px 0px 10px 0px #4194FF',
      }}
      loadingText={typeof children === 'string' ? children : ''}
      onClick={() => {

      }}
      {...props}
    >
      {children}
    </PrimaryButton>
  )
}


