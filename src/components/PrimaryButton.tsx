import { Button, ButtonProps, useColorMode, } from '@chakra-ui/react'

const PrimaryButton = ({ children, disabled, ...props }: ButtonProps) => {
    const { colorMode, } = useColorMode();

    return (
        <Button
            transition="all 200ms ease-in-out"
            transform="translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg)"
            color={`${colorMode}.textPrimary`}
            background={`${colorMode}.bgPrimary`}
            fontWeight={700}
            fontSize={{
                base: "14px",
            }}
            borderStyle={"dashed"}
            borderWidth={"1px"}
            borderColor={`${colorMode}.borderColor`}
            borderRadius="8px"
            disabled={disabled}
            // _disabled={{
            //     backgroundColor: '#A2A2A2',
            //     background: "#E2E2E2",
            //     color: '#A1A1A1',
            // }}
            _hover={{
                boxShadow: '0px 0px 10px 0px #4194FF',
                transform: "translate3d(0px, -8px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg)"
            }}
            loadingText={typeof children === 'string' ? children : ''}
            {...props}
        >
            {children}
        </Button>
    )
}

export default PrimaryButton
