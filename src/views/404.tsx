import { Box, Flex, Image, Text, useColorMode } from '@chakra-ui/react'
import MainLayout from '~/@ui/MainLayout';
import NotFoundIcon from '~/assets/images/404.png'

const NotFoundView = () => {
  const { colorMode, } = useColorMode();

  return (
    <MainLayout>
      <Flex
        flex={1}
        justifyContent="center"
        alignItems="center"
        h="100%"
        flexDir="column"
      >
        <Box h="20vh" />
        <Image
          src={NotFoundIcon}
          w={{ base: '415px', md: '185px' }}
          h={{ base: '66px', md: '60px' }}
          objectFit="contain"
        />
        <Text
          mt="16px"
          color={`${colorMode}.dotInActive`}
          fontSize="24px"
          fontWeight="700"
          lineHeight="28px"
          textTransform={"capitalize"}
        >
          Page Not Found
        </Text>
        <Box h="30vh" />
      </Flex>
    </MainLayout>
  )
}

export default NotFoundView
