
import '../../App.css';
import { VStack, useColorMode, Image, Text, Box, SimpleGrid, Button } from '@chakra-ui/react'
import { useEffect, useState } from 'react';

import ImageLogo from "~/assets/images/header/logo.png"
// import ImageBg from "~/assets/images/header/background.png"
import ImageBg from "~/assets/images/header/background1.jpg"
import ImageBg2 from "~/assets/images/header/background2.jpeg"
// import Image1 from "~/assets/images/header/img1.png"
import Image1 from "~/assets/images/header/img2.jpg"


import Ic1 from "~/assets/images/header/1.svg"
import Ic2 from "~/assets/images/header/2.svg"
import Ic3 from "~/assets/images/header/3.png"
import Ic4 from "~/assets/images/header/4.png"
import Ic5 from "~/assets/images/header/5.png"
import Ic6 from "~/assets/images/header/6.png"

import { useWindowSize } from '~/hooks/@global';


const TIME_OUT = 2000;

const DATA = [
  {
    description: [
      "Dexscreener/Dextools Listings",
      "CoinGecko/CMC Listings",
      "TOP 1 trending on Fantom"
    ]
  },
  {
    description: [
      "Make Fantom great again",
      "CEX Listings",
      "1M marketcap",
    ]
  },
  {
    description: [
      "Listing on top CEX",
    ]
  },
  {
    description: [
      "Iconic of Fantom Ecosystem",
    ]
  },
]

const ICONS = [
  {
    img: Ic1,
    href: 'https://spooky.fi/#/swap?outputCurrency=0x3bc34d8ace32d768a3f76e17aaef2b1d8f261e1d',
  },
  {
    img: Ic2,
    href: 'https://ftmscan.com/token/0x3bc34d8Ace32D768a3F76e17AAEF2B1D8f261e1D',
  },
  {
    img: Ic3,
    href: 'https://dexscreener.com/fantom/0xc5a18c888d7d5e8c3442b5da2e4d68e2ac7fca9f',
  },
  {
    img: Ic4,
    href: 'https://twitter.com/CekkeCronje',
  },
  {
    img: Ic5,
    href: 'https://t.me/cekkecronjechannel',
  },
  {
    img: Ic6,
    href: 'https://www.coingecko.com/en/coins/cekke-cronje',
  },
]

const HomeView = () => {

  const { colorMode } = useColorMode();
  const { width } = useWindowSize();
  const [isOpen, setIsOpen] = useState(false);
  const [selectItem, setSelectItem] = useState(null);

  useEffect(() => {
    let timerId;
    timerId = setInterval(() => {
      setIsOpen(true);
      return function cleanup() {
        clearInterval(timerId);
      };
    }, TIME_OUT);
  }, []);

  if (!isOpen) {
    return (
      <VStack
        w="full"
        minHeight={"100vh"}
        background={"#000"}
        justifyContent={"center"}
      >

        <Image
          src={ImageLogo}
          className='App-logo'
          alt="logo"
        />

      </VStack>
    )
  }

  return (
    <VStack
      w="full"
      spacing={0}
    >

      <Image
        src={ImageBg}
        w="full"
        height={{ base: "620px", lg: "auto" }}
        objectFit={"cover"}
      />

      <VStack
        bg="#1735f6"
        w="full"
        p={{ base: "30px 10px 10px 10px", md: "120px 20px 80px 20px" }}
      >

        <VStack
          w="full"
          maxW={{ md: "1140px", lg: "1300px" }}
          p={{ base: "20px 5px 20px 5px", md: "50px 60px 50px 70px" }}
        >
          <Text
            className='textPrimary'
            fontFamily={'RammettoOne'}
            maxW={{ md: "732px", lg: "1150px" }}
            textAlign={"center"}
            fontSize={{ base: "55px", md: "70px" }}
          >
            MEME OF FANTOM AND EVERYONE
          </Text>
        </VStack>

        <Text
          fontFamily={"Comic_Neue_Bold"}
          color={"#000"}
          fontWeight={600}
          fontSize={{ base: "16px", md: "19px" }}
          lineHeight={{ base: "24px", md: "32px" }}
          textAlign={"center"}
          maxW={{ lg: "1280px" }}
        >
          CEKKE - the vibrant meme coin igniting Fantom's ecosystem! Paying homage to the visionary Andre Cronje, Cekke Cronje is a community-driven project with no owner rights or holdings. Join us in celebrating innovation and supporting Fantom's growth!
        </Text>

        <SimpleGrid
          mt="30px"
          w="full"
          columns={{ base: 2 }}
        >

          <VStack
            w="full"
          >

            <Button
              fontFamily={'RammettoOne'}
              fontSize={"16px"}
              color={"#000"}
              background={"#bf047e"}
              border="3px solid #000"
              borderRadius={"20px"}
              p="30px 20px"
              _hover={{
                background: "#bf047e",
                transition: "background-color .1s .3s, color .1s .3s",
                animation: "anim-moema-1 .3s forwards",
              }}
              onClick={() => {
                window.open("https://dexscreener.com/fantom/0xc5a18c888d7d5e8c3442b5da2e4d68e2ac7fca9f")
              }}
            >
              Dexscreener
            </Button>

          </VStack>

          <VStack
            w="full"
          >

            <Button
              fontFamily={'RammettoOne'}
              fontSize={"16px"}
              color={"#000"}
              background={"#bf047e"}
              border="3px solid #000"
              borderRadius={"20px"}
              p="30px 20px"
              _hover={{
                background: "#bf047e"
              }}
              onClick={() => {
                window.open("https://ftmscan.com/tx/0x1926c450e48b9a1ab8bcbe9005865c5ae8406a782b57def9062d1ecf22d2bbd0")
              }}
            >
              Burned LP
            </Button>

          </VStack>

        </SimpleGrid>

      </VStack>

      <VStack
        w="full"
        position={"relative"}
      >

        {width > 920 &&
          <Image
            w="full"
            src={ImageBg2}
            position={"relative"}
          />
        }

        <SimpleGrid
          py={{ base: "60px", md: "30px", lg: "0px" }}
          px="10px"
          top={{ lg: "20%" }}
          position={{ lg: "absolute" }}
          w="full"
          spacing={{ base: 8, md: 2 }}
          columns={{ base: 1, md: 2 }}
        >

          <VStack
            w="full"
            alignItems={{ base: "center", md: "start" }}
            pl={{ lg: "90px" }}
          >

            <VStack
              w="full"
              maxW={410}
              border="2px solid #000"
              padding={{ base: "40px", lg: "50px 20px 50px 20px" }}
              borderRadius={"20px"}
              bg="#fff"
            >

              <Text
                className='textPrimary'
                fontFamily={'RammettoOne'}
                textAlign={"center"}
                fontSize={{ base: "40px", md: "50px" }}
              >
                Contract
              </Text>

              <Text
                textAlign={"center"}
                fontSize={"12px"}
              >
                0x3bc34d8ace32d768a3f76e17aaef2b1d8f261e1d
              </Text>

              <Image src={Image1} borderRadius={"32px"} />

            </VStack>

          </VStack>


          <VStack
            w="full"
            pr={{ lg: "90px" }}
            alignItems={{ base: "center", md: "end" }}
          >

            <VStack
              w="full"
              maxW={410}
              border="2px solid #000"
              padding={{ base: "10px", lg: "50px 20px 50px 20px" }}
              borderRadius={"20px"}
              bg="#fff"
            >

              <Text
                className='textPrimary'
                fontFamily={'RammettoOne'}
                textAlign={"center"}
                fontSize={{ base: "40px", md: "50px" }}
              >
                490T
              </Text>

              <Text
                mt="-20px"
                className='textSencondary'
                fontFamily={'RammettoOne'}
                textAlign={"center"}
                fontSize={{ base: "20px" }}
              >
                Total Supply
              </Text>

              <Text
                mt="20px"
                className='textPrimary'
                fontFamily={'RammettoOne'}
                textAlign={"center"}
                fontSize={{ base: "40px", md: "50px" }}
              >
                0%
              </Text>
              <Text
                mt="-20px"
                className='textSencondary'
                fontFamily={'RammettoOne'}
                textAlign={"center"}
                fontSize={{ base: "20px" }}
              >
                Tax
              </Text>

              <Button
                mt="10px"
                background={"rgb(242, 5, 68)"}
                border="3px solid #000"
                borderRadius={"20px"}
                p="30px 20px"
                _hover={{
                  background: "rgb(242, 5, 68)",
                }}
                onClick={() => {
                  window.open("https://spooky.fi/#/swap?outputCurrency=0x3bc34d8ace32d768a3f76e17aaef2b1d8f261e1d")
                }}
              >
                <Text
                  fontSize={"16px"}
                  className='textButton'
                  fontFamily={'RammettoOne'}
                >
                  Buy $CEKKE
                </Text>
              </Button>

            </VStack>

          </VStack>


        </SimpleGrid>


      </VStack>

      <VStack
        w="full"
        bg={"#1735f6"}
        p="120px 20px 80px 20px"
      >

        <Text
          fontFamily={'RammettoOne'}
          className='textRoadMap'
          fontSize={{ base: "52px", md: "70px" }}
          textAlign={"center"}
        >
          ROADMAP
        </Text>

        <SimpleGrid
          mt="100px"
          w="full"
          spacing={2}
          columns={{ base: 1, md: 2, lg: 4 }}
        >

          {DATA.map((item, idx) => {

            return (
              <VStack
                w="full"
                backgroundColor={"#f20544"}
                borderRadius={"20px"}
                borderStyle={"solid"}
                borderWidth={"2px 4px 4px 2px"}
                borderColor={"#000"}
                height={"300px"}
                justifyContent={"center"}
                onMouseOver={() => {
                  setSelectItem(idx);
                }}
                onMouseLeave={() => {
                  setSelectItem(null);
                }}
                cursor={{ lg: "pointer" }}
              >

                {selectItem === idx ?
                  <>
                    {item.description.map((itemSel, idxItem) => {
                      return (
                        <Text
                          key={idxItem}
                          fontFamily={'RammettoOne'}
                          fontSize={{ md: "15px" }}
                          lineHeight={"18px"}
                          textAlign={"center"}
                          color={"rgb(247, 239, 239)"}
                        >
                          {itemSel}
                        </Text>
                      )
                    })}
                  </>
                  :
                  <Text
                    fontFamily={'RammettoOne'}
                    fontSize={{ md: "45px" }}
                    textAlign={"center"}
                    color={"#000"}
                    textTransform={"uppercase"}
                  >
                    {`Phase ${idx + 1}`}
                  </Text>
                }

              </VStack>
            )
          })}

        </SimpleGrid>

      </VStack>

      <Box
        w="full"
        pt="50px"
        bg={"#1735f6"}
      >

        <div className="scroll">
          <div className="m-scroll">

            {["CEKKE", "DOGE", "SHIBA", "PEPE", "BONK", "FLOKI"].map((item, idx) => {
              return (
                <Text
                  key={idx}
                  color={"#1735f6"}
                  letterSpacing={"-3.4px"}
                  fontFamily={'RammettoOne'}
                  className='textScroll'
                  fontSize={{ base: "137px" }}
                  textAlign={"center"}
                  mx={{ base: "30px" }}
                >
                  {item}
                </Text>
              )
            })
            }

          </div>
        </div>

      </Box>

      <VStack
        w="full"
        py="50px"
        bg={"#1735f6"}
      >

        <VStack
          w="full"
          maxW={"1000px"}
        >

          <SimpleGrid
            w="full"
            spacing={4}
            columns={{ base: 2, md: 6 }}
          >

            {ICONS.map((item, idx) => {

              return (
                <VStack
                  w="full"
                  key={idx}
                  cursor={{ lg: 'pointer' }}
                  onClick={() => {
                    window.open(item.href)
                  }}
                  _hover={{
                    transform: "scale(0.9)",
                    transitionDuration: ".3s",
                  }}
                >
                  <Image
                    src={item.img}
                    w={{ base: "85px" }}
                    h={{ base: "85px" }}
                  />
                </VStack>
              )
            })
            }
          </SimpleGrid>

        </VStack>

      </VStack>

      <VStack
        w="full"
        p="20px"
        background={"#fb3404"}
      >

        <Text
          fontFamily={'RammettoOne'}
          className='textFooter'
          fontSize={{ md: "16px" }}
          textAlign={"center"}
        >
          All Rights Reserved © 2023 CEKKE CRONJE COMMUNITY
        </Text>

      </VStack>

    </VStack>
  )

}

export default HomeView;
